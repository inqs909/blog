---
title: The `ucrstats` R Package
author: Isaac Quintanilla Salinas
date: '2021-08-15'
slug: []
draft: true
categories:
  - EDU
  - R
tags: []
subtitle: ''
lastmod: '2021-08-15T07:44:06-07:00'
authorLink: ''
description: ''
hiddenFromHomePage: no
hiddenFromSearch: no
featuredImage: ''
featuredImagePreview: ''
toc:
  enable: yes
math:
  enable: no
lightgallery: no
license: ''
---

## Introduction {#intro}

During the [SMART Program](https://blog.inqs.info/2021-05-03-smart-program/), I mentored a student in creating an R package called the [`ucrstats`](https://ucrgradstat.github.io/ucrstats/) R Package.

## The Package

## Next Steps
